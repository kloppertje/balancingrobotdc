#include <Arduino.h>
#include <MPU6050.h>
#include <PID.h>
#include <Streaming.h>
#include "DCMotor.h"

// #include "driver/mcpwm_prelude.h" // Only works with new mcpwm driver, in ESP IDF > 5.0

// Based on earlier balancing robot code
// Goal: start with a control loop as simple as possible, to control a DC motor based on an IMU. 
// Later on I can always make it more complicated, that's the easy part. 


void readSensor();
void initSensor(uint8_t n);


// --- IMU
MPU6050 imu;

#define GYRO_SENSITIVITY 65.5


int16_t gyroOffset[3] = {0,0,0};
float accAngle = 0;
float filterAngle = 0;
float angleOffset = 0.0;
float gyroFilterConstant = 0.996;
float gyroGain = 1.0;

// --- PWM
DCMotor motorLeft(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM0A, MCPWM0B, 27, 14);

// --- Control loop 
#define dT_MICROSECONDS 5000
#define dT dT_MICROSECONDS/1000000.0

#define PID_ANGLE_MAX 50
PID pidAngle(cPID, dT, PID_ANGLE_MAX, -PID_ANGLE_MAX);

void setup() {
  Serial.begin(115200);

  // Gyro setup
  Wire.begin(21, 22, 400000UL);
  delay(100);
  Serial.println(imu.testConnection());
  imu.initialize();
  imu.setFullScaleGyroRange(MPU6050_GYRO_FS_500);
  initSensor(50);

  motorLeft.init();
  
  pidAngle.setParameters(2,1.0,0.075,15);  
}

void loop() {
  static unsigned long tLast = 0;
  float pidAngleOutput = 0;

  unsigned long tNow = micros();

  if (tNow-tLast > dT_MICROSECONDS) {
    readSensor();

    pidAngle.setpoint = 0;    
    pidAngle.input = filterAngle;
    pidAngleOutput = pidAngle.calculate();

    motorLeft.setDuty(50 + pidAngleOutput);

    Serial << filterAngle << "\t" << pidAngleOutput << endl;
  }
}


void readSensor() {
  int16_t ax, ay, az, gx, gy, gz;
  float deltaGyroAngle;

  imu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

  accAngle = atan2f((float) ay, (float) az) * 180.0/M_PI - angleOffset;
  deltaGyroAngle = ((float)((gx - gyroOffset[0])) / GYRO_SENSITIVITY) * dT * gyroGain;

  filterAngle = gyroFilterConstant * (filterAngle + deltaGyroAngle) + (1 - gyroFilterConstant) * (accAngle);
}

void initSensor(uint8_t n) {
  float gyroFilterConstantBackup = gyroFilterConstant;
  gyroFilterConstant = 0.8;
  for (uint8_t i=0; i<n; i++) {
    readSensor();
  }
  gyroFilterConstant = gyroFilterConstantBackup;

}