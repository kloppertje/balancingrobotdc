#ifndef DCMOTOR_H
#define DCMOTOR_H

#include "driver/mcpwm.h"

// See DCMotor.cpp for some explanations

#define DCMOTOR_PWM_FREQUENCY 20000

class DCMotor {
    public:
        DCMotor(mcpwm_unit_t mcpwmUnit, mcpwm_timer_t mcpwmTimer, mcpwm_io_signals_t mcpwmAPin, mcpwm_io_signals_t mcpwmBPin, int gpioA, int gpioB);
        void init(void);
        void setDuty(float duty);

    private:
        mcpwm_unit_t _mcpwmUnit;
        mcpwm_timer_t _mcpwmTimer;
        mcpwm_io_signals_t _mcpwmAPin;
        mcpwm_io_signals_t _mcpwmBPin;
        int _gpioA;
        int _gpioB;
};


#endif