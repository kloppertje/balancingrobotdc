#include <Arduino.h>
#include "DCMotor.h"

// Class for controlling a DC motor, using the MCPWM functionality of the ESP32
// MCPWM interfacing got an update in (I think) ESP IDF 5.0
// This class is written for versions before IDF 5.0
// Example code is included at the end of the .cpp file for IDF >=5.0 but I haven't tested / finalized this
//
// I use "diagnoal" switching of the H-bridge, i.e. a duty cycle smaller than 50 % means left and a duty cycle
// larger than 50 % means right. This gives a bit higher power dissipation/switching losses, but makes 
// control easier, and allows for energy being transported back to the power source :)
//
// Wouter Klop
// wouter@elexperiment.nl
// Sep 2023

DCMotor::DCMotor(mcpwm_unit_t mcpwmUnit, mcpwm_timer_t mcpwmTimer, mcpwm_io_signals_t mcpwmAPin, mcpwm_io_signals_t mcpwmBPin, int gpioA, int gpioB) {
    _mcpwmUnit = mcpwmUnit;
    _mcpwmTimer = mcpwmTimer;
    _mcpwmAPin = mcpwmAPin;
    _mcpwmBPin = mcpwmBPin;
    _gpioA = gpioA;
    _gpioB = gpioB;

}

void DCMotor::init() {
    mcpwm_gpio_init(_mcpwmUnit, _mcpwmAPin, _gpioA);
    mcpwm_gpio_init(_mcpwmUnit, _mcpwmBPin, _gpioB);


    mcpwm_config_t pwm_config;
    pwm_config.frequency = DCMOTOR_PWM_FREQUENCY;     
    pwm_config.cmpr_a = 50;                              //initial duty cycle of 50 %
    pwm_config.cmpr_b = 50;                              //initial duty cycle of 50 %
    pwm_config.counter_mode = MCPWM_UP_COUNTER;         //up counting mode
    pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
    mcpwm_init(_mcpwmUnit, _mcpwmTimer, &pwm_config);    //Configure PWM0A & PWM0B with above settings
    mcpwm_set_duty_type(_mcpwmUnit, _mcpwmTimer, MCPWM_OPR_A, MCPWM_DUTY_MODE_0); // Channel B is inverted version of channel A
    mcpwm_set_duty_type(_mcpwmUnit, _mcpwmTimer, MCPWM_OPR_B, MCPWM_DUTY_MODE_1);
}


void DCMotor::setDuty(float duty) {
    mcpwm_set_duty(_mcpwmUnit, _mcpwmTimer, MCPWM_OPR_A, duty);
    mcpwm_set_duty(_mcpwmUnit, _mcpwmTimer, MCPWM_OPR_B, duty);
}



// #define BDC_MCPWM_TIMER_RESOLUTION_HZ 10000000 // 10MHz, 1 tick = 0.1us
// #define BDC_MCPWM_FREQ_HZ             20000    // 20kHz PWM
// #define BDC_MCPWM_DUTY_TICK_MAX       (BDC_MCPWM_TIMER_RESOLUTION_HZ / BDC_MCPWM_FREQ_HZ) // maximum value we can set for the duty cycle, in ticks
// #define BDC_MCPWM_GPIO_A              27
// #define BDC_MCPWM_GPIO_B              14

// // Motor PWM setup for ESP IDF > 5.0
// mcpwm_timer_handle_t timer = NULL;
// mcpwm_timer_config_t timer_config = {
//     .group_id = 0,
//     .clk_src = MCPWM_TIMER_CLK_SRC_DEFAULT,
//     .resolution_hz = BDC_MCPWM_TIMER_RESOLUTION_HZ,
//     .period_ticks = BDC_MCPWM_DUTY_TICK_MAX,
//     .count_mode = MCPWM_TIMER_COUNT_MODE_UP,
// };
// ESP_ERROR_CHECK(mcpwm_new_timer(&timer_config, &timer));

// mcpwm_oper_handle_t oper = NULL;
// mcpwm_operator_config_t operator_config = {
//        .group_id = 0, // operator must be in the same group to the timer
// };
// ESP_ERROR_CHECK(mcpwm_new_operator(&operator_config, &oper));

// // Connect timer and operator
// ESP_ERROR_CHECK(mcpwm_operator_connect_timer(oper, timer));

// // Create comparator and generator from the operator
// mcpwm_cmpr_handle_t comparatorA = NULL;
// mcpwm_cmpr_handle_t comparatorB = NULL;
// mcpwm_comparator_config_t comparator_config = {
//     .flags.update_cmp_on_tez = true,
// };
// ESP_ERROR_CHECK(mcpwm_new_comparator(oper, &comparator_config, &comparatorA));
// ESP_ERROR_CHECK(mcpwm_new_comparator(oper, &comparator_config, &comparatorB));

// // set the initial compare value for both comparators
// mcpwm_comparator_set_compare_value(comparatorA, BDC_MCPWM_DUTY_TICK_MAX/2);
// mcpwm_comparator_set_compare_value(comparatorB, BDC_MCPWM_DUTY_TICK_MAX/2);

// // Create generator
// mcpwm_gen_handle_t generatorA = NULL;
// mcpwm_gen_handle_t generatorB = NULL;

// mcpwm_generator_config_t generator_config = {
//     .gen_gpio_num = BDC_MCPWM_GPIO_A,
// };
// ESP_ERROR_CHECK(mcpwm_new_generator(oper, &generator_config, &generatorA));
// generator_config.gen_gpio_num = BDC_MCPWM_GPIO_B;
// ESP_ERROR_CHECK(mcpwm_new_generator(oper, &generator_config, &generatorB));

// mcpwm_generator_set_actions_on_timer_event(generatorA,
//         MCPWM_GEN_TIMER_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, MCPWM_TIMER_EVENT_EMPTY, MCPWM_GEN_ACTION_HIGH),
//         MCPWM_GEN_TIMER_EVENT_ACTION_END());
// mcpwm_generator_set_actions_on_compare_event(generatorA,
//         MCPWM_GEN_COMPARE_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, comparatorA, MCPWM_GEN_ACTION_LOW),
//         MCPWM_GEN_COMPARE_EVENT_ACTION_END());
// mcpwm_generator_set_actions_on_timer_event(generatorB,
//         MCPWM_GEN_TIMER_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, MCPWM_TIMER_EVENT_EMPTY, MCPWM_GEN_ACTION_LOW),
//         MCPWM_GEN_TIMER_EVENT_ACTION_END());
// mcpwm_generator_set_actions_on_compare_event(generatorB,
//         MCPWM_GEN_COMPARE_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, comparatorB, MCPWM_GEN_ACTION_HIGH),
//         MCPWM_GEN_COMPARE_EVENT_ACTION_END());

// // Enable and start timer
// ESP_ERROR_CHECK(mcpwm_timer_enable(timer));
// ESP_ERROR_CHECK(mcpwm_timer_start_stop(timer, MCPWM_TIMER_START_NO_STOP));